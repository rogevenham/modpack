data:extend{{
    type = "int-setting",
    name = "highlight-maximum",
    setting_type = "runtime-global",
    default_value = 64,
    minimum_value = 1,
    maximum_value = 512
}}